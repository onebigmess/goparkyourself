FROM alpine:3.4

ENV PYTHONUNBUFFERED 1
#install python pip and nodejs
RUN apk add --update python         \
                     py-pip         \
                     nodejs         \ 
                     openssl        \ 
                     libpq          \
                     python-dev     \
                     postgresql     \
                     postgresql-dev \
                     gcc            \
                     musl-dev       \
                     git
#RUN wget https://bootstrap.pypa.io/ez_setup.py -O - | python
 
RUN mkdir /code
WORKDIR /code
#install django requirements
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/

#install bower
RUN npm install -g bower

