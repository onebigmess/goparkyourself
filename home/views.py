from django.shortcuts import render
from django.template import RequestContext, loader
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from .models import CCTVTable, Infringement, Parking
import json

# Create your views here.
def index(request):
    context = {'title':'OneBigMess'}
    return render(request, 'home/index.html', context)

def HeatMap(request):

    CCTVTable_list = CCTVTable.objects.order_by('Council_ID')
    Infringement_list = Infringement.objects.order_by('iNo')
    Infringement_day = Infringement.objects.filter(IssueDate__startswith="14/04/2016")
    template = loader.get_template('home/HeatMap.html')
    context = RequestContext(request,{'CCTVTable_list' : CCTVTable_list, 'Infringement_list' : Infringement_list, 'Infringement_day' : Infringement_day})
    return HttpResponse(template.render(context))


def DataMap(request):

    CCTVTable_list = CCTVTable.objects.order_by('Council_ID')
    Infringement_list = Infringement.objects.order_by('iNo')
    Infringement_day = Infringement.objects.filter(IssueDate__startswith="14/04/2016")

    template = loader.get_template('home/dataMap.html')

    context = RequestContext(request,{
            'CCTVTable_list' : CCTVTable_list,
            'Infringement_list' : Infringement_list,
            'Infringement_day' : Infringement_day,
        })

    return HttpResponse(template.render(context))

def parking(request):
    getSource = 'Ballarat'
    getClass = '*'
    getCost = '*'
    if request.GET.get('class'):
        getClass = request.GET.get('class')

    if request.GET.get('cost'):
        getCost = request.GET.get('cost')
   # if getClass.lower() != '*':
    #JSONdata = Parking.objects.filter(Data__features__properties__class = getClass)
    #JSONdata = JSONdata.all()
    JSONdata = Parking.objects.get(Source = 'Ballarat')
    
    features = JSONdata.Data['features']
    if getClass != '*':
        features = [f for f in features if 'CLASS' in f['properties'] and f['properties']['CLASS'] == getClass]

    if getCost != '*':
        features = [f for f in features if 'COST' in f['properties'] and f['properties']['COST'] == getCost]

    JSONout = {}
    JSONout['type'] = "FeatureCollection"
    JSONout['totalFeatures'] = len(features)
    JSONout['features'] = features
    return JsonResponse(JSONout,safe=False)

