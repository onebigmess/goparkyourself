from django.conf.urls import url
from . import views


urlpatterns = [
    #ex: /home/
    url('$', views.index, name='index'),
    url(r'^parking/(?P<Source>\w{0,50})/(?P<Class>\w{0,50})/$', views.parking, name='parking'),
]
