from django.contrib import admin
from .models import CCTVTable, Parking

admin.site.register(CCTVTable)
admin.site.register(Parking)
# Register your models here.
