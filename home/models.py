from __future__ import unicode_literals

from django.contrib.postgres.fields import JSONField
from django.db import models


class CCTVTable(models.Model):
    Council_ID = models.CharField(max_length=24)
    Location = models.CharField(max_length=100, blank=True)
    Type = models.CharField(max_length=40, blank=True)
    Site = models.CharField(max_length=60,  blank=True)
    Lat = models.CharField(max_length=24)
    Long = models.CharField(max_length=24)

class Infringement(models.Model):
    iNo = models.CharField(max_length=24)
    IssueDate = models.CharField(max_length=24)
    Offence = models.CharField(max_length=255)
    Lat = models.CharField(max_length=24)
    Long = models.CharField(max_length=24)

class Parking(models.Model):
    Source = models.CharField(max_length=255, default='Not Applicable')
    Data = JSONField()
    
    def __unicode__(self):
        return self.Data

class OccupancyDesc(models.Model):
    LocID = models.CharField(max_length=24)
    Street = models.CharField(max_length=60)
    From = models.CharField(max_length=60)
    To = models.CharField(max_length=60)
    From_Lat = models.CharField(max_length=24)
    From_Long = models.CharField(max_length=24)
    To_Lat = models.CharField(max_length=24)
    To_Long = models.CharField(max_length=24)
    Side = models.CharField(max_length=5)
    Capacity = models.IntegerField()

class OccupancyData(models.Model):
    LocID = models.CharField(max_length=24)
    DateTime = models.CharField(max_length=30)
    Utilisation = models.DecimalField(max_digits=50,decimal_places=2)
